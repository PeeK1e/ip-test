FROM golang:alpine AS build
WORKDIR /build
COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ip-test ./cmd/ip-test/main.go

FROM scratch
WORKDIR /app
COPY --from=build /build/ip-test ./ip-test

ENTRYPOINT ["./ip-test"]
