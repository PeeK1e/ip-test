package main

import (
	"flag"
	"io"
	"log"
	"net/http"
)

var port string

func init() {
	flag.StringVar(&port, "p", "8080", "port to listen")
}

func main() {
	http.HandleFunc("/", printIP)
	for {
		err := http.ListenAndServe(":"+port, nil)
		if err != nil {
			log.Printf("Server ran into an error %s :: rebooting", err)
		}
	}
}

func printIP(w http.ResponseWriter, r *http.Request) {
	clientData := (*r).RemoteAddr

	if (*r).Header.Get("X-Forwarded-For") != "" {
		clientData = (*r).Header.Get("X-Forwarded-For") + "\n"
	}

	_, err := io.WriteString(w, clientData)
	if err != nil {
		log.Printf("Could not send data %s", err)
	}
}
